FROM tomcat
MAINTAINER Sascha Peilicke <sascha.peilicke@hotel.de>

RUN ["rm", "-rf", "/usr/local/tomcat/webapps/ROOT"]
COPY lac-mock.war /usr/local/tomcat/webapps/ROOT.war

CMD ["catalina.sh", "run"]
