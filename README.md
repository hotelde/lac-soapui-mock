Location Autocomplete Service Mock
==================================

Generated with SoapUI. Currently covers some mobile and web cases around Berlin,
Fürth and Nürnberg.


Deployment
----------

A war file web application is exported via SoapUI. We build a docker container
running tomcat and serving the lac-mock.war web app exclusively. Check the 
Dockerfile for details. The following scripts may be used to build and deploy a
container image:

    ./bin/docker-build
    ./bin/docker-run

A simple test script is available too:

    ./bin/check

The generated docker container will listen on localhost:8082 on the docker host. That
you should set up a reverse proxy for proper external access. Here's how to do it with
apache (assuming you are root on Ubuntu). First of all install and start apache and
enable required modules:

    $ apt-get install apache2
    $ service apache2 start
    $ a2enmod proxy proxy_http

Paste the following content into /etc/apache2/sites-available/mock-hotel-de.conf
(and feel free to adjust it properly):

    #
    # Sascha Peilicke <sascha.peilicke@hotel.de>
    #
    <VirtualHost *:80>
        ServerAdmin sascha.peilicke@hotel.de
        ServerName  mock.hotel.de
        ServerAlias mock
        ProxyRequests off
        <Proxy *>
            Order deny,allow
            Allow from all
        </Proxy>
        ProxyPreserveHost on
        ProxyPass /lac http://localhost:8082/lac
        ProxyPassReverse /lac http://localhost:8082/lac
        AllowEncodedSlashes NoDecode
    </VirtualHost>

Save and close the file. Finally, you need to enable the vhost site configuration and
reload apache config:

    $ a2ensite mock-hotel-de
    $ service apache2 reload

Now the LAC mock service can be reached externally on that host.


Development
-----------

Open the project XML file with SoapUI and edit to your heart's content. Don't
forget to expand the test suite. Create a pull request and have your changes
discussed and reviewed. The test suite inside the SoapUI project is configured
to run against a local mock service instance, so don't forget to run it.

To inspect the public LAC service at hotel.de/lac, you can use the following example:

    $ curl -v -L -G hotel.de/lac -d m=1 -d lng=en -d pf=Berlin
