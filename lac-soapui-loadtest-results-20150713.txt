Test Step,min,max,avg,last,cnt,tps,bytes,bps,err,rat
Request Mobile / Fürth / DE,93,602,189.64,531,77,1.25,224532,3663,0,0
Request Mobile / Berlin / DE,75,509,146.05,143,77,1.25,186109,3036,0,0
Request Mobile / Berlin / EN,67,693,175.24,173,77,1.25,225610,3681,0,0
Request Mobile / Nürnberg / DE,69,805,232.74,608,77,1.25,254639,4155,0,0
Request Mobile / Fürth / EN,57,812,231.67,111,77,1.25,250327,4084,0,0
Request Mobile / Nürnberg / EN,106,680,222.84,113,77,1.25,351813,5740,0,0
Request Fürth / DE,99,678,209.37,118,77,1.25,514822,8400,0,0
Request Fürth / EN,73,749,212.85,138,77,1.25,553014,9024,0,0
Request Berlin / DE,75,771,247.37,603,77,1.25,494032,8061,0,0
Request Berlin / EN,88,829,276.5,157,77,1.25,535150,8732,0,0
Request Nürnberg / DE,116,856,268.22,130,77,1.25,532378,8687,0,0
Request Nürnberg / EN,118,731,201.87,122,77,1.25,605374,9878,0,0
Request Empty,27,665,158.28,478,77,1.25,0,0,0,0
Request Mobile / EN,28,623,97.27,126,77,1.25,0,0,0,0
Request Mobile / Berlin,111,963,216.44,190,77,1.25,186109,3036,0,0
TestCase:,1202,10966,3086.41,3741,77,1.25,4913909,80185,0,0
